import 'dart:ui';

import 'package:flutter/material.dart';

Widget myAppBar() {
  return AppBar(
    centerTitle: true,
    title: Image.asset("assets/jogjastreamer.png", height: 16,),
    backgroundColor: Colors.orange,
  );
}
