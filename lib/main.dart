import 'package:flutter/material.dart';
import 'package:flutter_radio_player/flutter_radio_player.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:jogjastreamer/repository/repo.dart';
import 'package:jogjastreamer/ui/widgets.dart';
import 'model/mobile.dart';
import 'model/slider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'JogjaStreamer',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.orange,
        // This makes the visual density adapt to the platform that you run
        // the app on. For desktop platforms, the controls will be smaller and
        // closer together (more dense) than on mobile platforms.
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Radio Stations'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (iRn this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  FlutterRadioPlayer _flutterRadioPlayer = new FlutterRadioPlayer();

  void _incrementCounter(String stationsName, String stationsUri) async {
    await _flutterRadioPlayer.init(
        "JogjaStreamer", stationsName, stationsUri, "true");
    await _flutterRadioPlayer.play();
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: myAppBar(),
      body: Column(
        children: <Widget>[
          Container(
              constraints: BoxConstraints.expand(height: 185),
              child: imageSlider(context)),
          Padding(
            padding: const EdgeInsets.all(12.0),
            child: TextField(
              controller: _textController,
              decoration: InputDecoration(
                hintText: 'Search Station...',
              ),
              onChanged: onItemChanged,
            ),
          ),
          Expanded(
            child: ListView.separated(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: stations.length,
              separatorBuilder: (ctx, ix) {
                return Divider(
                  height: 2,
                );
              },
              itemBuilder: (context, index) {
                return ListTile(
                    leading: Image.network(
                      stations[index].imageURL,
                      fit: BoxFit.cover,
                      width: 100.0,
                    ),
                    title: Text(stations[index].name),
                    onTap: () {
                      _incrementCounter(
                          stations[index].name, stations[index].streamURL);
                    });
              },
            ),
          )
        ],
      ),
      drawer: Drawer(
        child: ListView(
          children: <Widget>[
            DrawerHeader(
              child: Text('Drawer Header'),
              decoration: BoxDecoration(
                color: Colors.orange,
              ),
            ),
            /*Container(
              height: 160,
              decoration: BoxDecoration(
                  image: DecorationImage(
                image: AssetImage(
                  "assets/head-jogjastreamer.png",
                ),
                fit: BoxFit.fill,
              )),
            ),*/
            ListTile(
              leading: Icon(Icons.featured_play_list_outlined),
              title: Text('Playlist'),
            ),
            ListTile(
              leading: Icon(Icons.location_city),
              title: Text('mobile_city'),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              leading: Icon(Icons.leaderboard),
              title: Text('mobile_ranking'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            Divider(),
            ListTile(
              leading: Icon(Icons.info),
              title: Text('About'),
              onTap: () {
                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        child: Container(
          height: 100.0,
          child: Text("nama Stasiun"),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => setState(() {}),
        tooltip: 'Increment Counter',
        child: Icon(Icons.play_arrow),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    ); // This trailing comma makes auto-formatting nicer for build methods.
  }

  var tempStation = new List<Station>();
  var stations = new List<Station>();
  TextEditingController _textController = TextEditingController();

  _getStations() {
    Repository.stations().then((response) {
      setState(() {
        tempStation = response.station;
        stations = tempStation;
      });
    });
  }

  @override
  void initState() {
    super.initState();
    _getStations();
    _getSlider();
  }

  @override
  void dispose() {
    super.dispose();
  }

  onItemChanged(String value) {
    setState(() {
      stations = tempStation
          .where((string) =>
              string.name.toLowerCase().contains(value.toLowerCase()))
          .toList();
    });
  }

  Swiper imageSlider(context) {
    return new Swiper(
      autoplay: true,
      itemBuilder: (BuildContext context, int index) {
        for (int i = index; i < sliders.length; i++) {
          return Image.network(
            sliders[i].image,
            fit: BoxFit.fitHeight,
          );
        }
      },
      itemCount: sliders.length,
      viewportFraction: 0.8,
      scale: 0.9,
    );
  }

  var sliders = new List<SliderStation>();

  void _getSlider() {
    Repository.slider().then((response) {
      setState(() {
        sliders = response.station;
      });
    });
  }
}
