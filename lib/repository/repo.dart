import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:jogjastreamer/model/slider.dart';
import 'api.dart';
import 'package:jogjastreamer/model/about.dart';
import 'package:jogjastreamer/model/mobile.dart';

class Repository {
  static final Api apiClient = Api(httpClient: http.Client());


  static Future<About> about() async {
    return await apiClient.getAbout();
  }

  static Future<Mobile> stations() async {
    return await apiClient.getStations();
  }

  static Future<Slider> slider() async {
    return await apiClient.getSlider();
  }

}