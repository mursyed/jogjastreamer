import 'package:jogjastreamer/model/about.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:jogjastreamer/model/mobile.dart';
import 'package:jogjastreamer/model/slider.dart';
import 'package:meta/meta.dart';

class Api {
  final _baseUrl = 'http://jogjastreamers.com/m/';
  final http.Client httpClient;

  Api({
    @required this.httpClient,
  }) : assert(httpClient != null);

  Future<About> getAbout() async {
    var url = _baseUrl + 'about.php';
    var response = await this.httpClient.get(url);

    if (response.statusCode != 200) throw new Exception('error getting about');

    final jsonData = jsonDecode(response.body);
    return About.fromJsonMap(jsonData);
  }

  Future<Mobile> getStations() async {
    var url = _baseUrl + 'mobile_list.php';
    var response = await this.httpClient.get(url);

    if (response.statusCode != 200)
      throw new Exception('error getting stations');

    final jsonData = jsonDecode(response.body);
    return Mobile.fromJson(jsonData);
  }

  Future<Slider> getSlider() async {
    var url = _baseUrl + 'slider.php';
    var response = await this.httpClient.get(url);

    if (response.statusCode != 200)
      throw new Exception('error getting stations');

    final jsonData = jsonDecode(response.body);
    return Slider.fromJson(jsonData);
  }
}
