# jogjastreamer

JOGJASTREAMERS adalah salah satu dari situs penyedia jasa multimedia live streaming untuk stasiun-stasiun radio di kota Yogyakarta dan sekitarnya. Dimana setiap radio tersebut memiliki berbagai macam format siaran mulai contemporary hit radio, news, dangdut, budaya jawa, oldies music dan lain sebagainya.JOGJASTREAMERS berdiri pada tahun 2007 sebagai salah satu divisi dari internet service provider PT Jembatan Citra Nusantara yang saat sudah memiliki berbagai macam layanan internet dari mulai infrastruktur sampai dengan konten.Dengan banyaknya komunitas pelajar, mahasiswa dan eksekutif muda yang pernah belajar dan berkarya di kota Yogyakarta tentunya kangen akan situasi kota Yogyakarta. Kehadiran layanan JOGJASTREAMERS ini menjadi obat penawar rasa rindu bagi semua orang yang pernah tinggal di Yogyakarta dan ingin mengetahui kondisi kota Yogyakarta melalui live streaming radio yang bisa diakses melalui jalur internet.Pada akhirnya harapan JOGJASTREAMERS sebagai penyedia jasa live streaming ini adalah setiap stasiun radio di kota Yogyakarta pada khususnya bisa dinikmati siarannya di seluruh Nusantara bahkan nantinya di seluruh dunia melalui jalur internet

## Getting Started

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
